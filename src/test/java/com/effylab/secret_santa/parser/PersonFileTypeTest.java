package com.effylab.secret_santa.parser;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 8:13:59 PM
 * @version 1.0
 */
public class PersonFileTypeTest {
    
    @Test
    public void parse_CSV_1 () {
        FileType fileType = FileType.parse("csv_file.csv");
        assertEquals(FileType.CSV, fileType);
    }

    @Test
    public void parse_CSV_2 () {
        FileType fileType = FileType.parse(" csv ");
        assertEquals(FileType.CSV, fileType);
    }

    @Test
    public void parse_UNDEFINED_1 () {
        FileType fileType = FileType.parse("undefined_file.txt");
        assertEquals(FileType.UNDEFINED, fileType);
    }

    @Test
    public void parse_UNDEFINED_2 () {
        FileType fileType = FileType.parse(null);
        assertEquals(FileType.UNDEFINED, fileType);
    }

    @Test
    public void parse_UNDEFINED_3 () {
        FileType fileType = FileType.parse("   ");
        assertEquals(FileType.UNDEFINED, fileType);
    }
}
