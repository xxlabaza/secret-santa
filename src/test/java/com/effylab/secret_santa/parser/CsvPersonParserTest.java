package com.effylab.secret_santa.parser;

import com.effylab.secret_santa.Person;
import java.io.IOException;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 8:14:41 PM
 * @version 1.0
 */
public class CsvPersonParserTest {
    
    private final PersonParser personParser;
    
    private final String PERSON_LIST_1_FILE_NAME;

    private final String PERSON_LIST_2_FILE_NAME;
    
    private final String PERSON_LIST_3_FILE_NAME;
    
    private final String PERSON_LIST_INVALID_1_FILE_NAME;
    
    private final String PERSON_LIST_INVALID_2_FILE_NAME;

    public CsvPersonParserTest () {
        personParser = new CsvPersonParser();
        
        PERSON_LIST_1_FILE_NAME = getClass().getClassLoader().getResource("persons_list_1.csv").getPath();
        PERSON_LIST_2_FILE_NAME = getClass().getClassLoader().getResource("persons_list_2.csv").getPath();
        PERSON_LIST_3_FILE_NAME = getClass().getClassLoader().getResource("persons_list_3.csv").getPath();
        PERSON_LIST_INVALID_1_FILE_NAME = getClass().getClassLoader().getResource("persons_list_invalid_1.csv").getPath();
        PERSON_LIST_INVALID_2_FILE_NAME = getClass().getClassLoader().getResource("persons_list_invalid_2.csv").getPath();
    }
    
    @Test
    public void parse_ValidPersonList_1 () 
    throws IOException {
        List<Person> persons = personParser.parse(PERSON_LIST_1_FILE_NAME);
        
        assertNotNull(persons);
        assertFalse(persons.isEmpty());
        assertEquals(persons.size(), 4);
    }
    
    @Test
    public void parse_ValidPersonList_2 () 
    throws IOException {
        List<Person> persons = personParser.parse(PERSON_LIST_2_FILE_NAME);
        
        assertNotNull(persons);
        assertFalse(persons.isEmpty());
        assertEquals(persons.size(), 2);
    }
    
    @Test
    public void parse_ValidPersonList_3 () 
    throws IOException {
        List<Person> persons = personParser.parse(PERSON_LIST_3_FILE_NAME);
        
        assertNotNull(persons);
        assertFalse(persons.isEmpty());
        assertEquals(persons.size(), 3);
    }
    
    @Test
    public void parse_InvalidPersonList_1 () 
    throws IOException {
        List<Person> persons = personParser.parse(PERSON_LIST_INVALID_1_FILE_NAME);
        
        assertNotNull(persons);
        assertTrue(persons.isEmpty());
    }
    
    @Test
    public void parse_InvalidPersonList_2 () 
    throws IOException {
        List<Person> persons = personParser.parse(PERSON_LIST_INVALID_2_FILE_NAME);
        
        assertNotNull(persons);
        assertTrue(persons.isEmpty());
    }
}
