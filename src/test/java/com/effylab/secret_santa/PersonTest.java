package com.effylab.secret_santa;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 8:10:42 PM
 * @version 1.0
 */
public class PersonTest {
    
    private final Person PERSON_1 = new Person("Name #1", "name_1@mail.ru");
    
    private final Person PERSON_1_CLONE = new Person("Name #1", "name_1@mail.ru");
    
    private final Person PERSON_2 = new Person("Name #2", "name_2@mail.ru");
    
    @Test
    public void equals () {
        assertEquals(PERSON_1, PERSON_1_CLONE);
        assertNotEquals(PERSON_1, PERSON_2);
    }
}
