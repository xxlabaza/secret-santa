package com.effylab.secret_santa;

import com.effylab.secret_santa.parser.FileType;
import java.io.IOException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 11:11:33 PM
 * @version 1.0
 */
public class SettingsTest {

    @Test
    public void loadSettings () 
    throws IOException {
        String[] args = {
            "--template",       getClass().getClassLoader().getResource("template.ftl").getPath(),
            "--persons-list",    getClass().getClassLoader().getResource("persons_list_1.csv").getPath(),
            "--username",       "my_username",
            "--password",       "my_password"
        };
        
        Settings settings = Settings.getInstance();
        settings.loadSettings(args);
        
        String templateFileName = settings.getTemplateFileName();
        assertNotNull(templateFileName);
        assertFalse(templateFileName.isEmpty());
        assertEquals(args[1], templateFileName);
        
        String personsListFileName = settings.getPersonsListFileName();
        assertNotNull(personsListFileName);
        assertFalse(personsListFileName.isEmpty());
        assertEquals(args[3], personsListFileName);
        
        FileType fileType = settings.getPersonsListFileType();
        assertNotNull(fileType);
        assertEquals(FileType.CSV, fileType);
        
        String username = settings.getUsername();
        assertNotNull(username);
        assertFalse(username.isEmpty());
        assertEquals(args[5], username);
        
        String password = settings.getPassword();
        assertNotNull(password);
        assertFalse(password.isEmpty());
        assertEquals(args[7], password);
    }
}
