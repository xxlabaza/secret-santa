package com.effylab.secret_santa;

import freemarker.template.TemplateException;
import java.io.IOException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 8:15:35 PM
 * @version 1.0
 */
public class TemplaterTest {
    
    public final Templater templater;
    
    public final String MESSAGE = "Hello, Bob Smith, you are a secret Santa! Your pair is - Emma Smith (emma_smith@mail.ru)";

    public TemplaterTest () 
    throws IOException {
        String templateFileName = getClass().getClassLoader().getResource("template.ftl").getPath();
        templater = new Templater(templateFileName);
    }
    
    @Test
    public void template () throws IOException, TemplateException {
        Person secretSanta = new Person("Bob Smith", "bob_smith@mail.ru");
        Person pair = new Person("Emma Smith", "emma_smith@mail.ru");
        secretSanta.setPair(pair);
        
        String message = templater.template(secretSanta);
        
        assertNotNull(message);
        assertFalse(message.isEmpty());
        assertEquals(MESSAGE, message);
    }
}
