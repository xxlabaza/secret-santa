package com.effylab.secret_santa.pair;

import com.effylab.secret_santa.Person;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 8:13:21 PM
 * @version 1.0
 */
public class SimplePairCreatorTest {
    
    private final PairCreator pairCreator;
    
    private final Person PERSON_1 = new Person("Name #1", "name_1@mail.ru");
    
    private final Person PERSON_2 = new Person("Name #2", "name_2@mail.ru");
    
    private final Person PERSON_3 = new Person("Name #3", "name_3@mail.ru");
    
    private final Person PERSON_4 = new Person("Name #4", "name_4@mail.ru");
    
    private final Person PERSON_5 = new Person("Name #5", "name_5@mail.ru");
    
    private final Person PERSON_6 = new Person("Name #6", "name_6@mail.ru");
    
    private final Person PERSON_7 = new Person("Name #7", "name_7@mail.ru");
    
    public SimplePairCreatorTest () {
        pairCreator = new SimplePairCreator();
    }

    @Test
    public void createPairs_SimplePairs () {
        List<Person> persons = Arrays.asList(
                PERSON_1,
                PERSON_2
        );
        
        pairCreator.createPairs(persons).stream().forEach((person) -> {
            if (person.getFullName().equals(PERSON_1.getFullName())) {
                assertEquals(person, PERSON_1);
                assertEquals(person.getPair(), PERSON_2);
            }
            if (person.getFullName().equals(PERSON_2.getFullName())) {
                assertEquals(person, PERSON_2);
                assertEquals(person.getPair(), PERSON_1);
            }
        });
    }
    
    @Test
    public void createPairs_MediumPairs () {
        List<Person> persons = Arrays.asList(
                PERSON_1,
                PERSON_2,
                PERSON_3,
                PERSON_4,
                PERSON_5,
                PERSON_6,
                PERSON_7
        );
        
        List<Person> hasSecretSanta = new ArrayList<>(persons.size());
        
        pairCreator.createPairs(persons).stream().forEach((person) -> {
            Person pair = person.getPair();
            assertNotNull(pair);
            assertFalse(hasSecretSanta.contains(pair));
            hasSecretSanta.add(pair);
        });
        
        assertEquals(persons.size(), hasSecretSanta.size());
    }
}
