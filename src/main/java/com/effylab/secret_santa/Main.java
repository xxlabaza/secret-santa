package com.effylab.secret_santa;

import com.effylab.secret_santa.parser.CsvPersonParser;
import com.effylab.secret_santa.parser.FileType;
import com.effylab.secret_santa.parser.PersonParser;
import freemarker.template.TemplateException;
import java.io.IOException;
import javax.mail.MessagingException;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 26, 2014 | 11:02:25 PM
 * @version 1.0
 */
public class Main {
    
    
    
    public static void main (String[] args) 
    throws IOException, TemplateException, MessagingException  {
        
        Settings settings = Settings.getInstance();
        settings.loadSettings(args);
        
        PersonParser personParser = createPersonsListParser();
        Templater templater = createTemplater();
        
        Application app = new Application(
                personParser,
                templater,
                settings.getPersonsListFileName(),
                settings.getUsername(),
                settings.getPassword(),
                settings.isDebug()
        );
        app.run();
    }
    
    private static PersonParser createPersonsListParser () {
        Settings settings = Settings.getInstance();
        FileType fileType = settings.getPersonsListFileType();
        switch (fileType) {
        case CSV:
            return new CsvPersonParser();
        case UNDEFINED:
        default:
            throw new IllegalArgumentException("Unsupported person list file format.");
        }
    }
    
    private static Templater createTemplater () 
    throws IOException {
        Settings settings = Settings.getInstance();
        String templaterFileName = settings.getTemplateFileName();
        return new Templater(templaterFileName);
    }
}
