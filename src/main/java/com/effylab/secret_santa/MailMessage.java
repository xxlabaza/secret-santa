package com.effylab.secret_santa;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 26, 2014 | 11:04:22 PM
 * @version 1.0
 */
public final class MailMessage {
    
    private static final long serialVersionUID = 1L;

    public static MailMessageBuilder create() {
        MailMessage mailMessage = new MailMessage();
        return mailMessage.new MailMessageBuilder();
    }
    
    private Authenticator authenticator;
    
    private InternetAddress fromEmailAddress;
    
    private InternetAddress recipientEmailAddress;
    
    private String title;
    
    private String text;
    
    private final Properties sessionProperties;
    
    private MailMessage () {
        sessionProperties = new Properties();
        sessionProperties.put("mail.smtp.auth", "true");
        sessionProperties.put("mail.smtp.starttls.enable", "true");
        sessionProperties.put("mail.smtp.host", "smtp.gmail.com");
        sessionProperties.put("mail.smtp.port", "587");
    }
    
    public void send() 
    throws MessagingException {
        Session session = Session.getInstance(sessionProperties, authenticator);
        
        Message message = new MimeMessage(session);
        message.setFrom(fromEmailAddress);
        message.setRecipient(Message.RecipientType.TO, recipientEmailAddress);
        message.setSubject(title);
        message.setText(text);

        Transport.send(message);
    }
    
    
    public class MailMessageBuilder {
        
        private MailMessageBuilder (){
        }
        
        public MailMessageBuilder setAuthentication (final String username, final String password) {
            MailMessage.this.authenticator = new Authenticator() {
                
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
            };
            return this;
        }
        
        public MailMessageBuilder setFromEmailAddress (String fromEmailAddress) 
        throws AddressException {
            MailMessage.this.fromEmailAddress = new InternetAddress(fromEmailAddress);
            return this;
        }
        
        public MailMessageBuilder setRecipientEmailAddress (String recipientEmailAddress) 
        throws AddressException {
            MailMessage.this.recipientEmailAddress = new InternetAddress(recipientEmailAddress);
            return this;
        }
        
        public MailMessageBuilder setTitle (String title) {
            MailMessage.this.title = title;
            return this;
        }
        
        public MailMessageBuilder setText (String text) {
            MailMessage.this.text = text;
            return this;
        }
        
        public MailMessage build () {
            if (MailMessage.this.authenticator == null) {
                throw new IllegalArgumentException("Username and password must be set.");
            }
//            if (MailMessage.this.fromEmailAddress == null) {
//                throw new IllegalArgumentException("");
//            }
            if (MailMessage.this.recipientEmailAddress == null) {
                throw new IllegalArgumentException("Recipient email address must be set.");
            }
            if (MailMessage.this.title == null) {
                throw new IllegalArgumentException("Message title must be set.");
            }
            if (MailMessage.this.text == null) {
                throw new IllegalArgumentException("Message body must be set.");
            }
            return MailMessage.this;
        }
    }
}
