package com.effylab.secret_santa;

import com.effylab.secret_santa.parser.FileType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 1:39:02 AM
 * @version 1.0
 */
public final class Settings {

    private static Settings instance;
    
    public static Settings getInstance () {
        if (instance == null) {
            synchronized (Settings.class) {
                if (instance == null) {
                    instance = new Settings();
                }
            }
        }
        return instance;
    }
    
    private String templateFileName;
    
    private String personsListFileName;
    
    private FileType personsListFileType;
    
    private String username;
    
    private String password;
    
    private boolean debug;
    
    private Settings () {
    }
    
    public void loadSettings (String[] args) 
    throws IOException {
        OptionParser parser = new OptionParser();
        
        parser.acceptsAll(Arrays.asList("help", "h", "?"), "Show this help.").forHelp();
        parser.acceptsAll(Arrays.asList("debug", "d"), "Flag for debug mode.");
        
        OptionSpec<File> templateFileOption = parser
                .acceptsAll(Arrays.asList("template", "t"), "Mail template file.")
                .withRequiredArg().required().ofType(File.class);
        OptionSpec<File> personsListFileOption = parser
                .acceptsAll(Arrays.asList("persons-list", "l"), "Person list with full names and emails.")
                .withRequiredArg().required().ofType(File.class);
        OptionSpec<String> usernameArg = parser
                .acceptsAll(Arrays.asList("username", "u"), "Username for authorization at email server.")
                .withRequiredArg().required().ofType(String.class);
        OptionSpec<String> passwordArg = parser
                .acceptsAll(Arrays.asList("password", "p"), "Password for authorization at email server.")
                .withRequiredArg().required().ofType(String.class);
        
        OptionSet options;
        try {
            options = parser.parse(args);
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
            System.out.println();
            parser.printHelpOn(System.out);
            System.exit(1);
            return;
        }
        
        if (options.has("help")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("debug")) {
            debug = true;
        }
        
        Path templateFile = postFileProcess(options, templateFileOption);
        if (!Files.exists(templateFile)) {
            throw new IOException("Template file '" + templateFile.toString() + "' doesn't exists.");
        }
        if (Files.isDirectory(templateFile)) {
            throw new IOException("Template file '" + templateFile.toString() + "' must not be a directory.");
        }
        templateFileName = templateFile.toString();
        
        Path personsListFile = postFileProcess(options, personsListFileOption);
        if (!Files.exists(personsListFile)) {
            throw new IOException("Persons list file doesn't exists.");
        }
        if (Files.isDirectory(personsListFile)) {
            throw new IOException("Persons list file must not be a directory.");
        }
        personsListFileName = personsListFile.toString();
        personsListFileType = FileType.parse(personsListFileName);
        
        username = options.valueOf(usernameArg);
        password = options.valueOf(passwordArg);
    }

    public String getTemplateFileName () {
        return templateFileName;
    }

    public String getPersonsListFileName () {
        return personsListFileName;
    }
    
    public FileType getPersonsListFileType () {
        return personsListFileType;
    }

    public String getUsername () {
        return username;
    }

    public String getPassword () {
        return password;
    }
    
    public boolean isDebug () {
        return debug;
    }

    private Path postFileProcess (OptionSet options, OptionSpec<File> optionSpec) {
        File file = options.valueOf(optionSpec);
        return file.getPath().startsWith("~") 
                ? Paths.get(file.getPath().replace("~", System.getProperty("user.home"))) 
                : file.toPath();
    }
}
