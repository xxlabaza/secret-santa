package com.effylab.secret_santa;

import com.effylab.secret_santa.pair.SimplePairCreator;
import com.effylab.secret_santa.parser.PersonParser;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 28, 2014 | 2:09:00 PM
 * @version 1.0
 */
public class Application {

    private final PersonParser personParser;
    
    private final Templater templater;
    
    private final String personsListFileName;
    
    private final String authenticationUsername;
    
    private final String authenticationPassword;
    
    private final boolean isDebug;
    
    public Application (PersonParser personParser, 
                        Templater templater, 
                        String personsListFileName, 
                        String authenticationUsername, 
                        String authenticationPassword, 
                        boolean isDebug)
    throws IOException {
        this.personParser = personParser;
        this.templater = templater;
        this.personsListFileName = personsListFileName;
        this.authenticationUsername = authenticationUsername;
        this.authenticationPassword = authenticationPassword;
        this.isDebug = isDebug;
    }
    
    public void run () 
    throws IOException, TemplateException, MessagingException {
        List<Person> pairs = new PairRandomizer(personParser, new SimplePairCreator()).createPairs(personsListFileName);

        if (isDebug) {
            runInDebugMode(pairs);
        } else {
            runInNormalMode(pairs);
        }
    }
    
    private void runInDebugMode (List<Person> pairs) {
        System.out.println("Debug mode is: ON");
        System.out.println("Generated pairs:");
        
        int some = 0;
        for (Person secretSanta : pairs) {
            int secretSantaNameLength = secretSanta.getFullName().length();
            if (secretSantaNameLength > some) {
                some = secretSantaNameLength;
            }
        };
        
        for (Person secretSanta : pairs) {
            Person pair = secretSanta.getPair();
            String str = "  %-" + some + "s - is secret Santa for -> %s\n";
            System.out.format(str, secretSanta.getFullName(), pair.getFullName());
        }
    }
    
    private void runInNormalMode (List<Person> pairs) 
    throws IOException, TemplateException, AddressException, MessagingException {
        for (Person person : pairs) {
            
            String message = templater.template(person);
            
            MailMessage mailMessage = MailMessage.create()
                    .setAuthentication(authenticationUsername, authenticationPassword)
                    .setRecipientEmailAddress(person.getEmail())
                    .setTitle("Привет, секретный Санта!")
                    .setText(message)
                    .build();

            mailMessage.send();
            System.out.println("Send email to " + person.getEmail());
        }
    }
}
