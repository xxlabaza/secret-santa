package com.effylab.secret_santa;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author Artem Labazin (xxlabaza@gmail.com)
 * @since Dec 27, 2014 | 2:41:26 AM
 * @version 1.0
 */
public final class Templater {

    private final Configuration configuration;

    private final Template template;

    public Templater(String templateFileName) 
    throws IOException {
        String templateContent = new String(Files.readAllBytes(Paths.get(templateFileName)));
        
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        String templateName = "mailTemplate";
        stringLoader.putTemplate(templateName, templateContent);
        
        configuration = new Configuration(Configuration.VERSION_2_3_21);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateLoader(stringLoader);
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        
        this.template = configuration.getTemplate(templateName);
    }

    public String template(Person person) 
    throws IOException, TemplateException {
        Map<String, Person> parameters = new HashMap<>(2);
        parameters.put("secretSanta", person);
        parameters.put("pair", person.getPair());

        final StringWriter stringWriter = new StringWriter();
        try (final BufferedWriter writer = new BufferedWriter(stringWriter)) {
            template.process(parameters, writer);
            writer.flush();
            return stringWriter.getBuffer().toString();
        }
    }
}
