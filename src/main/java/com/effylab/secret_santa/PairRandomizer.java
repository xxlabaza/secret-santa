package com.effylab.secret_santa;

import com.effylab.secret_santa.pair.PairCreator;
import com.effylab.secret_santa.parser.PersonParser;
import java.io.IOException;
import java.util.List;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 12:13:08 AM
 * @version 1.0
 */
public class PairRandomizer {
    
    private final PersonParser personReader;
    
    private final PairCreator pairCreator;
    
    public PairRandomizer (PersonParser personReader, PairCreator pairCreator) {
        if (personReader == null) {
            throw new IllegalArgumentException("Person reader must be set.");
        }
        this.personReader = personReader;
        
        if (pairCreator == null) {
            throw new IllegalArgumentException("Pair creator must be set.");
        }
        this.pairCreator = pairCreator;
    }
    
    public List<Person> createPairs (String fileName) throws IOException {
        List<Person> persons = personReader.parse(fileName);
        return ((persons != null) && !persons.isEmpty()) 
                ? pairCreator.createPairs(persons) 
                : null;
    }
}
