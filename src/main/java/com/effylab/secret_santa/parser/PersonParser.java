package com.effylab.secret_santa.parser;

import com.effylab.secret_santa.Person;
import java.io.IOException;
import java.util.List;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 12:19:05 AM
 * @version 1.0
 */
public interface PersonParser {

    List<Person> parse (String fileName) throws IOException;
}
