package com.effylab.secret_santa.parser;

import au.com.bytecode.opencsv.CSVReader;
import com.effylab.secret_santa.Person;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 12:20:56 AM
 * @version 1.0
 */
public class CsvPersonParser implements PersonParser {
    
    private final Pattern EMAIL_PATTERN;
    
    public CsvPersonParser () {
        EMAIL_PATTERN = Pattern.compile(
                "^" + //                 start of the line
                "[_A-Za-z0-9-\\+]+" + // must start with string in the bracket [ ], must contains one or more (+)
                "(" + //                 start of group #1
                "\\.[_A-Za-z0-9-]+" + //    follow by a dot "." and string in the bracket [ ], must contains one or more (+)
                ")*" + //                end of group #1, this group is optional (*)
                "@" + //                 must contains a "@" symbol
                "[A-Za-z0-9-]+" + //     follow by string in the bracket [ ], must contains one or more (+)
                "(" + //                 start of group #2 - first level TLD checking
                "\\.[A-Za-z0-9]+" + //      follow by a dot "." and string in the bracket [ ], must contains one or more (+)
                ")*" + //                end of group #2, this group is optional (*)
                "(" + //                 start of group #3 - second level TLD checking
                "\\.[A-Za-z]{2,}" + //      follow by a dot "." and string in the bracket [ ], with minimum length of 2
                ")" + //                 end of group #3
                "$" //                   end of the line
        );
    }

    @Override
    public List<Person> parse(String fileName) 
    throws IOException {
        CSVReader reader = new CSVReader(new FileReader(fileName), ';');
        return reader.readAll().stream()
                .filter((row) -> {
                    return isValidRow(row);
                })
                .map((row) -> {
                    return new Person(row[0].trim(), row[1].trim());
                })
                .collect(Collectors.toList());
    }
    
    private boolean isValidRow (String[] row) {
        return (row != null) && 
               (row.length == 2) && 
               !row[0].trim().isEmpty() && 
               EMAIL_PATTERN.matcher(row[1].trim()).matches();
    }
}
