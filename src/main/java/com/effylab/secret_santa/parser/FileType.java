package com.effylab.secret_santa.parser;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 2:33:20 AM
 * @version 1.0
 */
public enum FileType {

    CSV,
    UNDEFINED;
    
    public static FileType parse (String fileName) {
        if ((fileName == null) || fileName.trim().isEmpty()) {
            return UNDEFINED;
        }
        
        String fileExtension = fileName.contains(".") 
                ? fileName.substring(fileName.lastIndexOf(".") + 1).trim() 
                : fileName.trim();
        
        for (FileType type : values()) {
            if (fileExtension.equalsIgnoreCase(type.name())) {
                return type;
            }
        }
        return UNDEFINED;
    }
}
