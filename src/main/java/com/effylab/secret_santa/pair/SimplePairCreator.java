package com.effylab.secret_santa.pair;

import com.effylab.secret_santa.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 12:51:05 AM
 * @version 1.0
 */
public class SimplePairCreator implements PairCreator {
    
    private final Random randomGenerator;
    
    private final List<Person> alreadyHasSecretSanta;
    
    private List<Person> personsList;
    
    public SimplePairCreator () {
        alreadyHasSecretSanta = new ArrayList<>();
        randomGenerator = new Random();
    }

    @Override
    public List<Person> createPairs(List<Person> persons) {
        alreadyHasSecretSanta.clear();
        randomGenerator.setSeed(System.currentTimeMillis());
        personsList = new ArrayList<>(persons);
        
        List<Person> result = new ArrayList<>(personsList.size());
        for (Person secretSanta : personsList) {
            Person pair = findPair(secretSanta);
            alreadyHasSecretSanta.add(pair);
            secretSanta.setPair(pair);
            result.add(secretSanta);
        }
        
        return result;
    }
    
    private Person findPair (Person secretSanta) {
        List<Person> personsListClone = new ArrayList<>(personsList);
        
        personsListClone.remove(secretSanta);
        personsListClone.removeAll(alreadyHasSecretSanta);
        
        if (personsListClone.size() == 1) {
            return personsListClone.get(0);
        }
        
        int pairPersonIndex = randomGenerator.nextInt(personsListClone.size());
        return personsListClone.get(pairPersonIndex);
    }
}
