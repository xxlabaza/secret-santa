package com.effylab.secret_santa.pair;

import com.effylab.secret_santa.Person;
import java.util.List;

/**
 * 
 * 
 * @author  Artem Labazin (xxlabaza@gmail.com)
 * @since   Dec 27, 2014 | 12:47:24 AM
 * @version 1.0
 */
public interface PairCreator {

    List<Person> createPairs (List<Person> persons);
}
